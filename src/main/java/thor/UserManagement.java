/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */
package thor;

import java.util.ArrayList;

/**
 *
 * @author Nobpharat
 */
public class UserManagement {

    public static void main(String[] args) {
        User admin = new User("admin", "Administrator", "pass@1234", 'M', 'A');
        User user1 = new User("user1", "User1", "pass@1234", 'M', 'U');
        User user2 = new User("user2", "User2", "pass@1234", 'F', 'U');
        System.out.println(admin);
        System.out.println(user1);
        System.out.println(user2);
        User[] userArr = new User[3];
        userArr[0] = admin;
        userArr[1] = user1;
        userArr[2] = user2;
        System.out.println("\nPrint for userArr");
        for (int i = 0; i < userArr.length; i++) {
            System.out.println(userArr[i]);
        }
        System.out.println("\nPrint for ArrayList");
        ArrayList<User> userList = new ArrayList();
        userList.add(admin);
        System.out.println(userList.get(userList.size()-1)+"list size = "+userList.size());
        userList.add(user1);
        System.out.println(userList.get(userList.size()-1)+"list size = "+userList.size());
        userList.add(user2);
        System.out.println(userList.get(userList.size()-1)+"list size = "+userList.size());
        
        for(int i =0; i< userList.size();i++){
            System.out.println(userList.get(i));
        }
        User user3 = new User("user3", "User3", "pass@1234", 'M', 'U');
        userList.set(0, user3);
        userList.remove(userList.size()-1);
        System.out.println(userList.get(0));
        
        for(User u : userList){
            System.out.println(u);
        }
    }
}
